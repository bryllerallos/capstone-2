<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    public function status(){
	return $this->belongsTo('\App\Status');

	}

	public function slot(){
		return $this->belongsTo('\App\Slot');
	}

	public function user(){
		return $this->belongsTo('\App\User');

	}
}
