<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function slot(){
		return $this->belongsTo('\App\Slot');
	}
}

