<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Category;
use \App\Status;
use \App\Booking;
use \App\Slot;
use \App\User;
use Session;
use Auth;

class BookingController extends Controller
{
    public function addBook(){
    	$categories = Category::all();
    	$slots = Slot::where('availability', 'vacant')->get();
        


    	return view('userviews.addbook', compact('categories', 'slots'));
    }

    public function saveBook(Request $req){
        $slot = Slot::find($req->slot_id);
        $rules = array(
            "date" => "required",
            "brand" => "required",
            "plate" => "required");

        
       $this->validate($req, $rules);
      

        $newBook = new Booking;
        $newBook->date = $req->date;
        $newBook->brand = $req->brand;
        $newBook->plate = $req->plate;
        $newBook->user_id = Auth::user()->id;
        $newBook->category_id = $req->category_id;
        $newBook->slot_id = $req->slot_id;
        $newBook->status_id =1;
        $newBook->save();

        $slot->availability = 'Occupied';
        $slot->save();

        

        Session::flash("message", "Booking successful");
        return redirect()->back();

    
        
    }

    public function editBook($id){

        $book = Booking::find($id);
        $categories = Category::all();
        $slots = Slot::where('availability', 'vacant')->get();

        return view('userviews.editbook', compact('book', 'categories', 'slots'));

    }

    public function updateBook($id, Request $req){
        $booktoEdit = Booking::find($id);

        $booktoEdit->date = $req->date;
        $booktoEdit->brand = $req->brand;
        $booktoEdit->plate = $req->plate;
        $booktoEdit->category_id = $req->category_id;
        $booktoEdit->slot_id = $req->slot_id;
        $booktoEdit->save();

        return redirect('/mybooks');
    }

    public function myBooks(){
    		$bookings = Booking::where('user_id', Auth::user()->id)->get();

    		return view('userviews.mybooks', compact('bookings'));
    	}

    	


     public function addSlot(){
    	$slot = Slot::all();
    	$categories = Category::all();


    	return view('adminview.addslot', compact('slot', 'categories'));

    }

    public function store(Request $req){
    	//validate
    	$rules = array(
    		"name" => "required",
    		"category_id" => "required");
   
         

    	$this->validate($req, $rules);
    	$newSlot = new Slot;
    	$newSlot->name = $req->name;
    	$newSlot->category_id = $req->category_id;
        $newSlot->availability = $req->availability;

       

    	$newSlot->save();
    	Session::flash("message", "$newSlot->name has been added successfully");
    	return redirect()->back();

    	

    }

    public function allBooks(){
        $bookings = Booking::all();

            return view('adminview.bookings', compact('bookings'));


    }

    public function viewSlots(){
        $slots = Slot::all();

        return view('adminview.slots', compact('slots'));
    }

     public function editSlot($id){

        $slot = Slot::find($id);
        $categories = Category::all();
       

        return view('adminview.editSlot', compact('slot', 'categories'));

    }

    public function updateSlot($id, Request $req){
        $slotToUpdate = Slot::find($id);

        $slotToUpdate->name = $req->name;
        $slotToUpdate->category_id = $req->category_id;
        $slotToUpdate->availability = $req->availability;
        $slotToUpdate->save();

        Session::flash("message", "$slotToUpdate->name has been updated successfully");
        return redirect('/slots');
    }

    public function remove($id){

        $slotToRemove = Slot::find($id);
        $slotToRemove->delete();

        Session::flash("message", "$slotToRemove->name has been removed successfully");
        return redirect('/slots');
    }

    public function cancel($id){

        $bookToCancel = Booking::find($id);
        $bookToCancel->status_id = 2;
        $bookToCancel->save();

        Session::flash("message", "Booking has been cancelled successfully");
        return redirect('/mybooks');
    }

    public function approve($id){
        $booking = Booking::find($id);
        $booking->status_id = 4;
        $booking->save();

        Session::flash("message", "Booking has been approved");
        return redirect('/bookings');
    }

    public function deny($id){
        $booking = Booking::find($id);
        $booking->status_id = 3;
        $booking->save();

        Session::flash("message", "Booking has been denied");
        return redirect()->back();
    }
    
    public function filter($id){
            $bookings = Booking::where('category_id', $id)->get();
            $categories = Category::all();

            return view('adminview.bookings', compact('bookings', 'categories'));
        }

     public function sort($sort){
            $bookings = Booking::where('status_id', $id)->get();
            $statuses = Status::all();

            return view('adminview.bookings', compact('bookings', 'statuses'));
        }


} 



 

