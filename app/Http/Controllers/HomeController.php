<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use Auth;
use Session;
use \App\Role;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = User::where('id',Auth::user()->id)->firstOrFail();
        return view('home', compact('user'));
    }

    public function viewProfile(){
            $user = User::where('id',Auth::user()->id)->firstOrFail();         

            return view('userviews.profile', compact('user'));
        }

    public function allUsers(){
        $users = User::all();

        return view('adminview.allusers', compact('users'));

    }
    
        public function editUser($id){

        $user = User::find($id);

        return view('adminview.edituser', compact('user'));

    }

    public function updateUser($id, Request $req){

        $editUser = User::find($id);

        $editUser->firstname = $req->firstname;
        $editUser->lastname = $req->lastname;
        $editUser->address = $req->address;
        $editUser->phone = $req->phone;
        $editUser->email = $req->email;
        $editUser->save();


        Session::flash("message", "Client Updated Successfully");
        return redirect()->back();
    }

     public function createAdmin(){
        $role = Role::all();
        
        return view('adminview.createadmin', compact('role'));
    }

    public function saveAdmin(Request $req){
        $rules = array(
            "firstname" => "required",
            "lastname" => "required",
            "phone" => "required",
            "address" => "required",
            "username" => "required",
            "email" => "required",
            "password" => "required");
            

        
       $this->validate($req, $rules);
      

        $newUser = new User;
        $newUser->firstname = $req->firstname;
        $newUser->lastname = $req->lastname;
        $newUser->phone = $req->phone;
        $newUser->address = $req->address;
        $newUser->username = $req->username;
        $newUser->email = $req->email;
        $newUser->password = $req->password;
        $newUser->role_id =1;
        $newUser->save();

    

        Session::flash("message", "Admin Created Successfully");
        return redirect()->back();
 
    }  

    
    public function destroy($id){
        $userToDelete =  User::find($id);
        $userToDelete->delete();

        return redirect('/allusers');

    }    


}   
