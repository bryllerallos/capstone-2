@extends('layouts.app')
@section('content')


<h1 class="text-center">Add Parking Slot</h1>
@if(Session::has("message"))
<h4 class="text-center">{{Session::get("message")}}</h4>
@endif

<div class="col-lg-6 offset-lg-3">
	<form action="{{route('store')}}" method="POST">
		@csrf
		<div class="form-group">
			<label for="name">Slot Name:</label>
			<input type="text" name="name" class="form-control">
		</div>
		<div class="form-group">
			<label for="category_id">Type</label>
			<select name="category_id" class="form-control">
				@foreach($categories as $category)
				<option value="{{ $category->id }}">{{ $category->type }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="availability">Availability</label>
			<select name="availability" class="form-control">
				<option value="vacant">Vacant</option>
				<option value="occupied">Occupied</option>
			</select>
		</div>
		<div class="text-center">
			<button class="btn btn-primary" type="submit">Add</button>
		</div>
	</form>
			

</div>




















@endsection