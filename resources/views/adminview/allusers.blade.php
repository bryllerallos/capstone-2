@extends('layouts.app')
@section('content')



<h1 class="text-center py-5">Clients</h1>
<div class="container">
	<div class="row">
		<div class="col-lg-10 offset-lg-1">
			<table class="table table-striped border">
				<thead>
					<tr class="text-center">
						<th>First Name</th>
						<th>Last Name</th>
						<th>Address</th>
						<th>Phone</th>
						<th>Email</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
					<tr class="text-center">
						<td>{{$user->firstname}}</td>
						<td>{{$user->lastname}}</td>
						<td>{{$user->address}}</td>
						<td>{{$user->phone}}</td>
						<td>{{$user->email}}</td>


						<td class="d-flex">
							<a href="/edituser/{{$user->id}}" class="btn btn-info mr-3">Update</a>

							<form action="/deleteuser/{{$user->id}}" method="POST">
								@csrf
								@method ('DELETE')
								<button class="btn btn-danger">Delete</button>
							</form>

							{{-- <a href="/cancel/{{$order->id}}" class="btn btn-danger {{$order->status_id == 4 || $order->status_id == 2 ? "disable" : ""}}">Cancel</a> --}}
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>


		</div>
	</div>	
</div>




@endsection