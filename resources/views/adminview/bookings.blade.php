@extends('layouts.app')
@section('content')



<h1 class="text-center py-5">Bookings</h1>
<div class="container">
	<div class="row">
		<div class="col-lg-2">
			<h4 class="text-center">Sort by Status</h4>
			<ul class="list-group text-center">
				@foreach($bookings as $booking)
				<li class="list-group-item">
						<a class="text-primary" href="/bookings/{{$booking->status->name}}">{{$booking->status->name}}</a>
				</li>
				@endforeach
				<li class="list-group-item">
					<a class="text-primary" href="/bookings">All</a>
				</li>
				
			</ul>
			<hr>

			{{-- <h4 class="text-center">Sort by Type</h4>
			<ul class="list-group text-center">
				@foreach($bookings as $booking)
				<li class="list-group-item">
						<a class="text-primary" href="/bookings/{{$booking->category->name}}"></a>
				</li>
				<li class="list-group-item">
					<a class="text-primary" href="/bookings">All</a>
				</li>
				@endforeach
			</ul> --}}
		</div>
		<div class="col-lg-10">
			<table class="table table-striped border">
				<thead>
					<tr class="text-center">
						<th>Booking Id</th>
						<th>Booking Date</th>
						<th>Details</th>
						<th>Booking Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($bookings as $booking)
					<tr class="text-center">
						<td>{{$booking->created_at->format('U')}}-{{$booking->id}}</td>
						<td>{{$booking->created_at->diffForHumans()}}</td>
						<td>
							Brand: {{$booking->brand}},
							Plate Number: {{$booking->plate}}
							Date: {{$booking->date}}
							<br>

							<td>{{$booking->status->name}}</td>
							<td class="d-flex">

								<form action="/approve/{{$booking->id}}" method="POST">
									@csrf
									@method('PATCH')
									<button type="submit" class="btn btn-success mr-3">approve
									</button>
								</form>
								<form action="/deny/{{$booking->id}}" method="POST">
									@csrf
									@method('PATCH')
									<button type="submit" class="btn btn-danger">deny
									</button>
								</form>

								{{-- <a href="/approve/{{$booking->id}}" class="btn btn-success mr-3">Approve</a>
								<a href="/deny/{{$booking->id}}" class="btn btn-danger">Deny</a> --}}
								
								
								{{-- <a href="">Cancel</a> --}}
								{{-- <a href="/cancel/{{$order->id}}" class="btn btn-danger {{$order->status_id == 4 || $order->status_id == 2 ? "disable" : ""}}">Cancel</a> --}}
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>

			</div>
		</div>	
	</div>
























	@endsection