@extends('layouts.app')
@section('content')



<h1 class="text-center py-5">Create Admin</h1>
@if(Session::has("message"))
<h4 class="text-center">{{Session::get("message")}}</h4>
@endif
<div class="col-lg-6 offset-lg-3">
	<form action="/createadmin" method="POST">
		@csrf
		<div class="form-group">
			<label for="firstname">First Name</label>
			<input type="text" name="firstname" class="form-control">
		</div>

		<div class="form-group">
			<label for="lastname">Last Name:</label>
			<input type="text" name="lastname" class="form-control">
		</div>
		<div class="form-group">
			<label for="phone">Phone:</label>
			<input type="number" name="phone" class="form-control">
		</div>
		<div class="form-group">
			<label for="address">Address</label>
			<input type="text" name="address" class="form-control">
		</div>
		<div class="form-group">
			<label for="username">Username</label>
			<input type="text" name="username" class="form-control">
		</div>
		<div class="form-group">
			<label for="username">Email</label>
			<input type="text" name="email" class="form-control">
		</div>

		<div class="form-group">
			<label for="password">Password</label>
			<input type="text" name="password" class="form-control">
		</div>

		<div class="text-center">
			<button class="btn btn-info" type="submit">Submit</button>
		</div>
	</form>
</div>


<script type="text/javascript">
	
function checkDate() {
	let UserDate = document.getElementById("date").value;
	let ToDate = Date();


	if (UserDate < ToDate) {
    alert("Invalid Date")
    return false;
}
return true;}


</script>



@endsection