@extends('layouts.app')
@section('content')


<h1 class="text-center">Update Parking Slot</h1>
<div class="col-lg-6 offset-lg-3">
	<form action="/slots/{{$slot->id}}" method="POST">
		@csrf
		@method('PATCH')
		<div class="form-group">
			<label for="name">Slot Name:</label>
			<input type="text" name="name" class="form-control" value="{{$slot->name}}">
		</div>
		<div class="form-group">
			<label for="category_id">Type</label>
			<select name="category_id" class="form-control">
				@foreach($categories as $category)
				<option value="{{ $category->id }}">{{ $category->type }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="availability">Availability</label>
			<select name="availability" class="form-control">
				<option value="vacant">Vacant</option>
				<option value="occupied">Occupied</option>
			</select>
		</div>
		<div class="text-center">
			<button class="btn btn-primary" type="submit">Update</button>
		</div>
	</form>
			

</div>




















@endsection