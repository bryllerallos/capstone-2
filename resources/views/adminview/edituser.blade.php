@extends('layouts.app')
@section('content')



<h1 class="text-center py-5">Client Details</h1>
@if(Session::has("message"))
<h4 class="text-center">{{Session::get("message")}}</h4>
@endif
<div class="col-lg-6 offset-lg-3">
	<form action="/allusers/{{$user->id}}" method="POST">
		@csrf
		@method('PATCH')
		<div class="form-group">
			<label for="date">First Name</label>
			<input type="text" name="firstname" class="form-control" value="{{$user->firstname}}">
		</div>

		<div class="form-group">
			<label for="brand">Last Name:</label>
			<input type="text" name="lastname" class="form-control" value="{{$user->lastname}}">
		</div>
		<div class="form-group">
			<label for="plate">Address:</label>
			<input type="text" name="address" class="form-control" value="{{$user->address}}">
		</div>
		<div class="form-group">
			<label for="plate">Phone:</label>
			<input type="number" name="phone" class="form-control" value="{{$user->phone}}">
		</div>
		<div class="form-group">
			<label for="plate">Email:</label>
			<input type="text" name="email" class="form-control" value="{{$user->email}}">
		</div>
		
		<div class="text-center">
			<button class="btn btn-primary" type="submit">Update</button>
		</div>
	</form>

</div>



@endsection