@extends('layouts.app')
@section('content')


<h1 class="text-center py-5">Slots</h1>
@if(Session::has("message"))
<h4 class="text-center">{{Session::get("message")}}</h4>
@endif
<div class="container">
	<div class="row">
		<div class="col-lg-10 offset-lg-1">
			<table class="table table-striped">
				<thead>
					<tr class="text-center">
						<th>Slot Id</th>
						<th>Slot Name</th>
						<th>Slot Type</th>
						<th>Action</th>
					</tr>
				</thead>	
				<tbody>
					@foreach ($slots as $slot)
					<tr class="text-center">	
						<th>{{$slot->created_at->format('U')}}-{{$slot->id}}</th>
						<th>{{$slot->name}}</th>
						<th>{{$slot->category->type}}</th>
						<th>
								<form action="/deleteslot/{{$slot->id}}" method="POST">
									<a href="/editslot/{{$slot->id}}" class="btn btn-info mr-3">Update</a>
								@csrf
								@method ('DELETE')
								<button class="btn btn-danger">Remove</button>
							</form>
						</th>	
						
					</tr>
					@endforeach
				</tbody>

			</table>	

		</div>	

	</div>

</div>















@endsection