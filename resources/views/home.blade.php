@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <img class="w-100" src="images/parkingspace.jpg"></div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h1>Hello {{$user->firstname}}!</h1>
                    <p>Please take time to read our parking rules and regulations</p>

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
