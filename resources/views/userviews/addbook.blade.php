@extends('layouts.app')
@section('content')



<h1 class="text-center py-5">Book a Slot</h1>
@if(Session::has("message"))
<h4 class="text-center">{{Session::get("message")}}</h4>
@endif
<div class="col-lg-6 offset-lg-3">
	<form action="/addbook" method="POST">
		@csrf
		<div class="form-group">
			<label for="date">Date</label>
			<input type="date" name="date" class="form-control" id="date" min="{{now()->format('Y-m-d')}}">
		</div>

		<div class="form-group">
			<label for="brand">Car Brand:</label>
			<input type="text" name="brand" class="form-control">
		</div>
		<div class="form-group">
			<label for="plate">Plate Number:</label>
			<input type="text" name="plate" class="form-control">
		</div>
		<div class="form-group">
			<label for="category_id">Type</label>
			<select name="category_id" class="form-control">
				@foreach($categories as $category)
				<option value="{{ $category->id }}">{{ $category->type }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="slot_id">Slot</label>
			<select name="slot_id" class="form-control">
				@foreach($slots as $slot)
				<option value="{{ $slot->id }}">{{ $slot->name }}</option>
				@endforeach
			</select>
		</div>
		
		
		<div class="text-center">
			<button class="btn btn-primary" type="submit">Book</button>
		</div>
	</form>
</div>


<script type="text/javascript">
	
function checkDate() {
	let UserDate = document.getElementById("date").value;
	let ToDate = Date();


	if (UserDate < ToDate) {
    alert("Invalid Date")
    return false;
}
return true;}


</script>



@endsection