@extends('layouts.app')
@section('content')



<h1 class="text-center py-5">Book a Slot</h1>
@if(Session::has("message"))
<h4 class="text-center">{{Session::get("message")}}</h4>
@endif
<div class="col-lg-6 offset-lg-3">
	<form action="/mybooks/{{$book->id}}" method="POST">
		@csrf
		@method('PATCH')
		<div class="form-group">
			<label for="date">Date</label>
			<input type="date" name="date" class="form-control" value="{{$book->date}}">
		</div>

		<div class="form-group">
			<label for="brand">Car Brand:</label>
			<input type="text" name="brand" class="form-control" value="{{$book->brand}}">
		</div>
		<div class="form-group">
			<label for="plate">Plate Number:</label>
			<input type="text" name="plate" class="form-control" value="{{$book->plate}}">
		</div>
		<div class="form-group">
			<label for="category_id">Type</label>
			<select name="category_id" class="form-control">
				@foreach($categories as $category)
				<option value="{{ $category->id }}">{{ $category->type }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="slot_id">Slot</label>
			<select name="slot_id" class="form-control">
				@foreach($slots as $slot)
				<option value="{{ $slot->id }}"
					{{$slot->id == $book->slot->id ? "selected" : "" }}
					>{{ $slot->name }} ({{$slot->availability}})</option>
					}
				@endforeach
			</select>
		</div>
		
		
		<div class="text-center">
			<button class="btn btn-primary" type="submit">Update</button>
		</div>
	</form>

</div>



@endsection