@extends('layouts.app')
@section('content')



	<h1 class="text-center py-5">My Books</h1>
	@if(Session::has("message"))
	<h4 class="text-center">{{Session::get("message")}}</h4>
	@endif


	<div class="container">
		<div class="row">
			<div class="col-lg-10 offset-lg-1">
				<table class="table table-striped border">
					<thead>
						<tr class="text-center">
							<th>Booking Id</th>
							<th>Booking Date</th>
							<th>Details</th>
							<th>Booking Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($bookings as $booking)
						<tr class="text-center">
							<td>{{$booking->created_at->format('U')}}-{{$booking->id}}</td>
							<td>{{$booking->created_at->diffForHumans()}}</td>
							<td>
								
								Brand: {{$booking->brand}}
								Plate Number: {{$booking->plate}}
								Date: {{$booking->date}}
								Slot: {{$booking->slot->name}}
								<br>
								
							<td>{{$booking->status->name}}</td>
							<td class="d-flex">
								<a href="/editbook/{{$booking->id}}" class="btn btn-info mr-3">Update</a>

								<form action="/cancel/{{$booking->id}}" method="POST">
								@csrf
								@method('PATCH')
								<button class="btn btn-danger {{$booking->status_id == 2 ? "disable" : ""}}">Cancel</button>
							</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>


			</div>
		</div>	
	</div>




@endsection