<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



//admin



Route::post('/addslot', 'BookingController@store')->name('store');

Route::get('/addslot', 'BookingController@addSlot');

Route::get('/bookings', 'BookingController@allBooks');

Route::get('/allusers', 'HomeController@allUsers');

Route::get('/edituser/{id}', 'HomeController@editUser');

Route::patch('/allusers/{id}', 'HomeController@updateUser');

Route::post('/createadmin', 'HomeController@saveAdmin');

Route::get('/createadmin', 'HomeController@createAdmin');

Route::delete('/deleteuser/{id}', 'HomeController@destroy');

Route::get('slots', 'BookingController@viewSlots');

Route::get('/editslot/{id}', 'BookingController@editSlot');

Route::patch('/slots/{id}', 'BookingController@updateSlot');

Route::delete('/deleteslot/{id}', 'BookingController@remove');

//to deny booking

Route::patch('/deny/{id}', 'BookingController@deny');

//to approve booking

Route::patch('/approve/{id}', 'BookingController@approve');

Route::get('/bookings/{id}', 'BookingController@filter');

Route::get('/bookings/sort/{sort}', 'BookingController@sort');




//user

Route::post('/addbook', 'BookingController@saveBook');

Route::get('/addbook', 'BookingController@addBook');

Route::get('/mybooks', 'BookingController@myBooks');

Route::get('/profile', 'HomeController@viewProfile');

Route::get('/editbook/{id}', 'BookingController@editBook');

Route::patch('/mybooks/{id}', 'BookingController@updateBook');

Route::patch('/cancel/{id}', 'BookingController@cancel');

